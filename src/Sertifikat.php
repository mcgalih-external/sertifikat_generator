<?php
namespace MCGalih\Serti;

use Intervention\Image\ImageManagerStatic as ImageManagerStatic;
use Intervention\Image\Image as Image;

class Sertifikat{
    private $image;

    public function __construct($templatePath)
    {
        $this->image = ImageManagerStatic::make($templatePath);
    }

    public function text($text, $location, $font_prop){
        $font_prop = array_merge(["file" => 1, "size" => 12, "color" => "#000000", "align" => "left", "valign" => "bottom", "angle" => 0], $font_prop);
        $this->image->text($text, $location[0], $location[1], function($font_config) use($font_prop){
            $file = (substr_count($font_prop["file"], "@", 0, 1) > 0) ? storage_path(substr($font_prop["file"], 1)) : $font_prop["file"];
            $font_config->file($file);
            $font_config->size($font_prop["size"]);
            $font_config->color($font_prop["color"]);
            $font_config->align($font_prop["align"]);
            $font_config->valign($font_prop["valign"]);
            $font_config->angle($font_prop["angle"]);
        });
    }

    public function insert($source, $position, $location){
        $position = is_null($position) ? "top-left" : $position;
        $location = is_null($location) ? [0, 0] : $location;
        $this->image->insert($source, $position, $location[0], $location[1]);
    }

    public function image() : Image{
        return $this->image;
    }

    public function text_mapping($mappings){
        foreach($mappings as $mapping){
            $this->text($mapping["text"], $mapping["location"], $mapping["font_prop"]);
        }
    }

    public function json_mapping($jsonString){
        $mappings = json_decode($jsonString, true);
        $this->text_mapping($mappings);
    }

    public static function AddText($templatePath, $text, $location, $font_prop) : Sertifikat{
        $ins = new Sertifikat($templatePath);
        $ins->text($text, $location, $font_prop);
        return $ins;
    }

    public static function AddTextMapping($templatePath, $mappings) : Sertifikat{
        $ins = new Sertifikat($templatePath);
        $ins->text_mapping($mappings);
        return $ins;
    }

    public static function MapText($data, $jsonString){
        $assoc = json_decode($jsonString, true);
        $assoc = array_map(function($item)use($data){
            $item["text"] = (key_exists($item["name"], $data)) ? $data[$item["name"]] : $item["text"];
            return $item;
        }, $assoc);
        return json_encode($assoc);
    }
}
